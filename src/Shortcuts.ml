type t

external shortcuts :
  < capture : bool option ; shouldHandleEvent : bool option ; target : 'target option > Js.t -> t
  = "Shortcuts"
  [@@bs.new] [@@bs.module "shortcuts"]

external shortcuts_add : t -> < shortcut : string ; handler : 'event -> 'res' > Js.t -> unit = "add"
  [@@bs.send]

external shortcuts_remove : t -> < shortcut : string ; handler : ('event -> 'res) option > Js.t -> unit
  = "remove"
  [@@bs.send]

external shortcuts_reset : t -> unit -> unit = "reset" [@@bs.send]

external accelerator : string -> string = "shortcut2accelerator" [@@bs.module "shortcuts"] [@@bs.scope "Shortcut"]

external symbols : string -> string = "shortcut2symbols" [@@bs.module "shortcuts"] [@@bs.scope "Shortcut"]

let make ?capture ?target ?shouldHandleEvent () = shortcuts [%bs.obj { capture; target; shouldHandleEvent }]

let add sequence handler shortcuts = shortcuts_add shortcuts [%bs.obj { shortcut = sequence; handler }]

let remove sequence ?handler shortcuts = shortcuts_remove shortcuts [%bs.obj { shortcut = sequence; handler }]

let reset shortcuts = shortcuts_reset shortcuts ()
